<?php
namespace tests\functional;


use models\ProductModel;

class CartCest
{
    /**
     * @param \FunctionalTester $i
     * @throws \Exception
     */
    public function testGoToCart(\FunctionalTester $i): void
    {
       $i->amOnPage('/');

      $i->click('a[href="/cart"]');

      $i->see('Shopping Cart','.breadcrumb li.active');
    }


    /**
     * @param \FunctionalTester $i
     * @throws \Exception
     */
    public function testGoToEmptyCart(\FunctionalTester $i): void
    {
        $i->amOnPage('/');

        $i->dontSee('in cart');

        $i->click('a[href="/cart"]');

        $i->see('Shopping Cart','.breadcrumb li.active');

        $i->see('Your cart is empty', '.cart-empty');

        $i->dontSee('Check Out');
    }


    /**
     * @param \FunctionalTester $i
     * @throws \Exception
     */
    public function testGoToNotEmptyCart(\FunctionalTester $i): void
    {
        $productId = 5;

        /** @var ProductModel $product */
        $product = ProductModel::find()->where(['id' => $productId])->one();

        $i->amOnPage('/');

        $i->sendAjaxPost('/cart/add',[
            'product' => $productId,
        ]);

        $result = json_decode($i->grabResponse(),1);

        $i->assertArrayHasKey('error',$result);

        $i->assertFalse($result['error']);

        $i->click('a[href="/cart"]');

        $i->see('Shopping Cart','.breadcrumb li.active');

        $i->seeElement('tr.cart_menu');

        $i->see($product->name);

        $i->see('Check Out');
    }

    /**
     * @param \FunctionalTester $i
     * @throws \Exception
     */
    public function testGoToNotEmptyCart2(\FunctionalTester $i): void
    {
        $productId = 5;

        /** @var ProductModel $product */
        $product = ProductModel::find()->where(['id' => $productId])->one();

        $i->amOnPage('/');

        $i->sendAjaxPost('/cart/add',[
            'product' => $productId,
        ]);

        $result = json_decode($i->grabResponse(),1);

        $i->assertArrayHasKey('error',$result);

        $i->assertFalse($result['error']);

        $i->amOnPage('/');

        $i->see('in cart', 'a[data-id=' . $productId .']');

        $i->click('a[data-id=' . $productId .']');

        $i->see('Shopping Cart','.breadcrumb li.active');

        $i->seeElement('tr.cart_menu');

        $i->see($product->name);

        $i->see('Check Out');
    }

}