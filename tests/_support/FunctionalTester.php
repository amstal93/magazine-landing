<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class FunctionalTester extends \Codeception\Actor
{
    use _generated\FunctionalTesterActions;

   /**
    * Define custom actions here
    */

   public function sendAjaxPost($uri, $params = null)
   {
       $params['_csrf'] = $params['_csrf'] ?? $this->getCsrf();
       $this->sendAjaxPostRequest($uri, $params);
   }

    public function getCsrf(): string
   {
       return (string)$this->grabAttributeFrom('meta[name="_csrf"]','content');
   }
}
